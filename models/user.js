//modulo moogoose
const mongoose = require('mongoose');
//Schema es una propiedad
const Schema = mongoose.Schema;
//UserSchema
let UserSchema = new Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    telefono: {
        type: String,
        required: true
    },
    direccion: {
        type: String,
        required: true
    },
    arte: {
        type: String,
        required: true
    },
    cine: {
        type: String,
        required: true
    },
    musica: {
        type: String,
        required: true
    },
    editable: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('User', UserSchema);