export class User {
    constructor (
        public _id: number = Math.floor(Math.random () * 100),
        public first_name : string = "",
        public last_name : string = "",
        public email : string = "",
        public telefono : string = "",
        public direccion : string = "",
        public arte : string = "",
        public cine : string = "",
        public musica : string = "",
        public editable : boolean = false
    ){
     

    }
}
