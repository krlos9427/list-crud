import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { User } from './user';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  users: User[] =[
    //arreglo vacio
    /*
    new User(1, 'carlos','Almanza', 'caabcarlos27@gmail.com'),
    new User(2, 'karen','feria', 'kferia@gmail.com'),
    new User(3, 'Luis','Almanza', 'suministro@gmail.com')
    */
  ];
  
  constructor(
    private _userService: UserService
  ) { }

  ngOnInit() {
    this.getUsers();
  }
getUsers(){
  this._userService.getUsers()
    .then(users => this.users = users);
  }
  create(user:User){
   this.users.push(user);
   this._userService.create(user)
   //vamos a obtener una promesa
   .then(status => this.getUsers())
   .catch(err =>console.log(err));

  }
  destroy(user:User){
   // const i = this.users.indexOf(user);
    //this.users.splice(i, 1);
    this._userService.detroy(user)
    .then(status => this.getUsers())
    .catch(err =>console.log(err));
   }
   update(user){

    this._userService.update(user)
    .then(status => this.getUsers())
    .catch(err =>console.log(err));
     /*console.log(user);
    const i = this.users.indexOf(user.orginal);
    this.users[i]=user.edited;
 */
   }
}
